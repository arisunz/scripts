--
-- autofarmer on command. attach a router and send message "FARM_BEGIN" via rednet.
--
-- farm MUST be equal in size, e.g. 9x9, 5x5 etc.
--
-- layout should be as follows:
--
-- XXXXXP
-- XXXXX
-- XXXXX
-- XXXXX
-- XXXXX
-- F
-- 
-- X: farmland or water
-- P: produce chest
-- F: fuel chest, above which the robot should be placed,
--    facing towards the farm
--
-- licensed under CC0 1.0, go nuts
--

-- change these if you need to
FARM_WIDTH = 9
MIN_FUEL_LEVEL = 500 -- refuel self if fuel is below this level
MIN_FUEL_ITEMS = 10  -- pick up fuel if item count is below this number

function reapNextTile()
    turtle.digDown()
    turtle.placeDown()
    turtle.forward()
end

function reapRow()
    for i=1,FARM_WIDTH - 1 do
        reapNextTile()
    end
    turtle.digDown()
    turtle.placeDown()
end

function reapAllRows()
    local turnRight = true;
    for i=1,FARM_WIDTH do
        reapRow()
        if turnRight then
            turtle.turnRight() 
            turtle.forward()
            turtle.turnRight()
        else
            turtle.turnLeft()
            turtle.forward()
            turtle.turnLeft()
        end
        turnRight = not turnRight
    end
end

function resetPosition()
    for i=1,2 do
        for j=1,FARM_WIDTH do
            turtle.forward()
        end
        turtle.turnRight()
    end
end

function reapFarm()
    turtle.select(2)
    turtle.forward()
    reapAllRows()
end

function refuelSelf()
    turtle.select(1)

    -- TODO: check items are actually combustable?
    
    local count = turtle.getItemCount()
    if count < MIN_FUEL_ITEMS then
        turtle.suckDown(MIN_FUEL_ITEMS)
    end
    
    local fuel = turtle.getFuelLevel()
    if fuel < MIN_FUEL_LEVEL then
        turtle.refuel()
    end
end

function storeProduce()
    for i=2,16 do
        turtle.select(i)
        turtle.dropDown()
    end
end

function main()
    rednet.open("left")
    while true do
        local id, msg = rednet.receive()
        if msg == "FARM_BEGIN" then
            refuelSelf()
            reapFarm()
            storeProduce()
            resetPosition()
        end        
    end
end

main()
